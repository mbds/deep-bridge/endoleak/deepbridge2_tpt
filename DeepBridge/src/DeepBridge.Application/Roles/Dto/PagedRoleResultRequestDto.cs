﻿using Abp.Application.Services.Dto;

namespace DeepBridge.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

