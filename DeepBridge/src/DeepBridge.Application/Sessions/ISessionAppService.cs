﻿using System.Threading.Tasks;
using Abp.Application.Services;
using DeepBridge.Sessions.Dto;

namespace DeepBridge.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
