﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace DeepBridge
{
    /// <summary>
    /// Service that will execute our script
    /// </summary>
    public class ExecuteProgrammeService
    {
        /// <summary>
        /// Allow to execute cmd and execute a commande here allow to execute python script
        /// </summary>
        /// <param name="programmeName">Name of .py script</param>
        /// <param name="args">argument to take</param>
        /// <returns></returns>
        public async static Task<string> run_cmd(string programmeName, string args)
        {
            var process = new Process();
            process.StartInfo.FileName = "cmd.exe";
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.RedirectStandardInput = true;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.UseShellExecute = false;
            process.Start();
            await process.StandardInput.WriteLineAsync($"{programmeName} {args}");
            await process.StandardInput.FlushAsync();
            process.StandardInput.Close();
            var line = await process.StandardOutput.ReadToEndAsync();
            return line;
        }
    }
}
