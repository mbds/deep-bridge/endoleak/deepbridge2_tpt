﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using DeepBridge.Controllers;

namespace DeepBridge.Web.Controllers
{
    [AbpMvcAuthorize]
    public class HomeController : DeepBridgeControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
