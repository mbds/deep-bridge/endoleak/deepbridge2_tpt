﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace DeepBridge.Web.Views
{
    public abstract class DeepBridgeViewComponent : AbpViewComponent
    {
        protected DeepBridgeViewComponent()
        {
            LocalizationSourceName = DeepBridgeConsts.LocalizationSourceName;
        }
    }
}
