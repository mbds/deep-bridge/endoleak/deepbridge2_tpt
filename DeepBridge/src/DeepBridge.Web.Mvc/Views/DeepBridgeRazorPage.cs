﻿using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;
using Microsoft.AspNetCore.Mvc.Razor.Internal;

namespace DeepBridge.Web.Views
{
    public abstract class DeepBridgeRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected DeepBridgeRazorPage()
        {
            LocalizationSourceName = DeepBridgeConsts.LocalizationSourceName;
        }
    }
}
