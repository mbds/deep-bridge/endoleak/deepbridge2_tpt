﻿using System.Collections.Generic;
using DeepBridge.Roles.Dto;

namespace DeepBridge.Web.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }
    }
}