using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace DeepBridge.EntityFrameworkCore
{
    public static class DeepBridgeDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<DeepBridgeDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<DeepBridgeDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
