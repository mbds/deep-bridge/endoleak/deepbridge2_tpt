﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using DeepBridge.Configuration;

namespace DeepBridge.Web.Host.Startup
{
    [DependsOn(
       typeof(DeepBridgeWebCoreModule))]
    public class DeepBridgeWebHostModule: AbpModule
    {
        private readonly IWebHostEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public DeepBridgeWebHostModule(IWebHostEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DeepBridgeWebHostModule).GetAssembly());
        }
    }
}
