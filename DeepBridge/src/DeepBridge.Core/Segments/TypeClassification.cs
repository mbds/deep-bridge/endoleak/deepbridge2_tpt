﻿using Abp.Domain.Entities;
using DeepBridge.Predictions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DeepBridge.Segments
{
    public class TypeClassification: Entity<int>
    {
        public TypeClassification()
        {
            Models = new HashSet<Model>();
        }

        public string Libelle { get; set; }
        public string Description { get; set; }

        // Proprietée de navigation

        [NotMapped]
        public virtual ICollection<Model> Models { get; set; }
    }
}
