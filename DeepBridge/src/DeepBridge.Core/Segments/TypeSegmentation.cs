﻿using Abp.Domain.Entities;
using DeepBridge.Predictions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DeepBridge.Segments
{
    public class TypeSegmentation : Entity<int>
    {
        public TypeSegmentation()
        {
            Models = new HashSet<Model>();
        }

        public string Libelle { get; set; }
        public string Description { get; set; }

        // Proprietée de navigation
        [NotMapped]
        public virtual ICollection<Model> Models { get; set; }
    }
}
