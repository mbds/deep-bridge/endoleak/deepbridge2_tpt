﻿using Abp.Domain.Entities;
using DeepBridge.Predictions;
using System.Collections.Generic;

namespace DeepBridge.Personnes
{
    public class Admin : Entity<int>
    {
        public Admin()
        {
            CasEtudes = new HashSet<CasEtude>();
        }

        public string Email { get; set; }
        public int UtilisateurId { get; set; }

        // Proprietée de navigation
        public virtual Utilisateur Utilisateur { get; set; }
        public virtual ICollection<CasEtude> CasEtudes { get; set; }
    }
}
