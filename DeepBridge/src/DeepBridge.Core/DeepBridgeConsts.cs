﻿namespace DeepBridge
{
    public class DeepBridgeConsts
    {
        public const string LocalizationSourceName = "DeepBridge";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
