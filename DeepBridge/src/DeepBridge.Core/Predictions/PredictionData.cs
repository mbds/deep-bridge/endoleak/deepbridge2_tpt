﻿using Abp.Domain.Entities;
using DeepBridge.Personnes;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DeepBridge.Predictions
{
    public class PredictionData : Entity<int>
    {
        public PredictionData()
        {
            Utilisateurs = new HashSet<Utilisateur>();
        }
        public string Libelle { get; set; }
        public string Link { get; set; }
        public int CasEtudeId { get; set; }
        public int PredictionId { get; set; }
        public int UtilisateurId { get; set; }


        // Proprietée de navigation
        public virtual CasEtude CasEtude { get; set; }
        public virtual Prediction Prediction { get; set; }
        public virtual Utilisateur Utilisateur { get; set; }

        [NotMapped]
        public virtual ICollection<Utilisateur> Utilisateurs { get; set; }
    }
}
