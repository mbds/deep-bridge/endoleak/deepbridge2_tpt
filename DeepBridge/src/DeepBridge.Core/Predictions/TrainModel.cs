﻿using Abp.Domain.Entities;
using System.Collections.Generic;

namespace DeepBridge.Predictions
{
    public class TrainModel : Entity<int>
    {
        public string Link { get; set; }
        public string Libelle { get; set; }
        public int CasEtudeId { get; set; }

        public virtual CasEtude CasEtude { get; set; }
    }
}
