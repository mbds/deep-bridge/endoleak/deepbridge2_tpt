﻿using Abp.Domain.Entities;

namespace DeepBridge.Predictions
{
    public class Prediction : Entity<int>
    {
        public string Link { get; set; }
        public string Type { get; set; }
        public int PredictionDataId { get; set; }

        // Proprietée de navigation
        public virtual PredictionData PredictionData { get; set; }
    }
}
