﻿using Abp.Authorization;
using DeepBridge.Authorization.Roles;
using DeepBridge.Authorization.Users;

namespace DeepBridge.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
