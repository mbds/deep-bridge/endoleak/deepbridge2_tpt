# DeepBridge2_TPT

MBDS project 2019-2020
# Prérequis 

Nous avons testé notre pipeline, pour cela nous avons voulu appliquer une tache de classification sur le jeu de données publiques
*MRNet Dataset* ([à telecharger ici](https://stanfordmlgroup.github.io/competitions/mrnet/ ))

Pour tester nos programmes il faut:
- Avoir une version de Python à jour (> Python 3.6)
- Avoir `pip` installé
- Exécuter le fichier *install.py* depuis un terminal ou un cmd pour télécharger les librairies utiles.

# Implémentation de la pipeline v0

Pour la version 0, la pipeline est implémentée sous forme d'un programme "main" qui effetue des imports conditionnels selon des 
paramètres entrées par l'utilisateur.

Le répértoire ***scripts*** de ce projet contient:
- Un module pour lire des données de classification
- Un module pour appliquer une suite de transformations a des données de classification
- Un module pour créer un dataloader
- Un module pour entrainer des modèles 
- Un module pour évaluer des modèles
- Un module pour créer un MRNet
- *model.py*: c'est le programme "main" qui crée le modèle, il importe les modules précédents selon des paramètres à entrer par l'utilisateur
- *predict.py*: apres avoir crée un modèle avec *model.py*, on peut l'appliquer avec *predict.py*

Paramètres de *model.py*:
- `task`: *segmentation* ou *classification*
- `directory` : chemin du jeu de données d'apprentissage
- `type` : format des données, pour le moment *.npy* ou *.nii.gz* (nifti)
- `prefix_name` : nom à donner au modèle qui sera sauvegardé
- `transforms` : *transforms_seg* ou *transforms_classif*
- `seg_method` : *unet* ou *attentionUnet*
- `classif_method` : *alexNet* ou *MRnet*
- `batch` : nombre de batch 
- `epochs` : nombre d'epochs
- `lr` : learning rate initial
- `flush_history`: 0 ou 1, si 1 supprime tous les logs si un dossier logs est dèja existant
- `save_model` : 0 ou 1, si 1 le modèle crée est sauvé
- `patience` : condition d'arret
- `log_every` : l'ajout de ce parametre permet de garder un oeil sur l'avancement de l'apprentissage


# Tests des modèles

Le fichier *test_classif.ipynb* est un notebook jupyter, qui reprend les modules et les codes
précédent pour les tester de facon interractive. Pour les explorer:
- upload le jeu de données ainsi que tous les scripts .py sur son drive Google
- ouvrir les notebook avec Google Colab
- mount son drive
- se placer dans le repertoire ou se trouvent les scripts

