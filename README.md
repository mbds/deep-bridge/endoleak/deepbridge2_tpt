# DeepBridge2_TPT

MBDS project 2019-2020

***Classification***

Readme dans le dossier

***Core Projet***

DeepBridge

**Prerequis**

Git
Visual Studio Community (De preference 2019)
Sql Server Express (c'est une version plus legere), permettra de créer la base en local. lien : https://www.microsoft.com/fr-fr/sql-server/sql-server-downloads

Sql Server Management Studio (SSMS pour les intimes), permet de visualiser et manipuler la BD. lien : https://docs.microsoft.com/fr-fr/sql/ssms/download-sql-server-management-studio-ssms?redirectedfrom=MSDN&view=sql-server-ver15#download-ssms



**Mise en place de l'environement**


Installez Sql Serveur Express


Clonez le repo git


Modifiez la ConnectionStrings, présente dans les fichiers "appsettings.json" dans les projets : Migrator, Web.Host et Web.MVC par votre chaîne de connexion.
Exemple : "Default": "Server=DESKTOP-ODPVA55\SQLEXPRESS;Database=DeepBridgeDb;Trusted_Connection=True;"


Ouvre la fenêtre "Package Manager Console" et sélectionnez le projet EntityFrameworkCore


Exécutez la commande "Update-Database"
Ceci aura pour effet de créer la base de donnée en Code First.
Pour visualiser la BD, vous pouvez vous connecter au serveur grâce a SSMS


Définissez le projet Web.Host en tant que projet de démarrage par défaut


En haut, vérifiez que vous avez bien "DeepBridge.Web.Host" en lancement de l'application


Vous pouvez maintenant lancer votre application, une page swagger devrait s'afficher avec l'API exposée par l'application.

**Explication**

Dans la partie src/Application la classe ExecuteProgrammeService permet de lancer de un script python en CMD sur le serveur

Cependant pas de service et de DTO crée pour les objets de prediction (base de donnée lié au programme)

*Partie Application*

Regroupe le DTO et les differents service lié a la BD et autres

*Partie Core*

Reprensente les entité de notre base crée en code first (entité modelisé dans le dossier Prediction)

*EntityFramework*

represente le parametrage de entityFramework et le seed

*Web Core*

Represente la gestion d'authentification et de catch et transmission de token

*Web Host*

Permets de lancer l'api et son parametrage
